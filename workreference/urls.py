from django.urls import path

from . import views

urlpatterns = [
    path('', views.workreference, name='workreference'),
]
