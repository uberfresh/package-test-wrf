from setuptools import find_packages, setup

REPO_URL = 'https://gitlab.com/uberfresh/package-test-wrf'

setup(
    name='workreference-check',
    version='2.2.4',
    packages = [
        'workreference'
    ],
    include_package_data=True,
    license='BSD-2-Clause',
    description='A cool, modern and responsive django admin application',
    url=REPO_URL,
    author='uberfresh',
    author_email='batuhan@pescheck.nl',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: BSD-2-Clause',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Framework :: Django :: 2.1',
        'Framework :: Django :: 2.2',
        'Framework :: Django :: 3.0',
        'Framework :: Django :: 3.1',
        'Framework :: Django :: 4.0',
        'Topic :: Software Development',
        'Topic :: Software Development :: User Interfaces',
    ],
)
